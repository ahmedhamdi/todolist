import React, { Component } from 'react'
import { Button } from 'antd';
import { Checkbox } from 'antd';

export default class RenderItem extends Component {
  render() {
      const {data, onEdit, onRemove,oncomleted,filter}=this.props
    return (        
      <span>
          {data.todo}
          <Button type="danger" shape="circle" icon="delete" onClick={onRemove} /> 
          <Button  shape="circle" icon="edit" onClick={onEdit} />  
        { filter==null && <Checkbox onChange={oncomleted}>done</Checkbox>}
      </span>
        
    )
  }
}
