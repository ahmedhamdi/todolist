import React, { Component } from 'react'
import { todoObject } from './todoinput';
import { Field, reduxForm } from 'redux-form';
import Renderfield from "../../component_data";
import {Form,Button} from "antd";
import RenderItem from "./todoitem";
 class TodoContiant extends Component {
    constructor (props){
        super(props);
        this.state= { 
            editIndex: -1,
            items:[],
            filter: null
        }
    }
    submitfunction(values){
        const {items, editIndex} = this.state;
      if(values.todo!=undefined)
      {
        if(editIndex > -1){
            this.setState({
                items: items.map((d, index)=>(index==editIndex ? {todo: values.todo}:d)),
                editIndex: -1
            })
        }else{
            this.setState({
                items: [...items, {todo: values.todo, completed: false}]
            })
        }
        this.setTodo('')
    
    }
}
    removeToDo(index){
        this.setState({
            items: this.state.items.filter((_, i)=>(i!=index))
        })
    
    }
    oncomleted(index){
        this.setState({
            items:this.state.items.map((d,i)=>(i!=index ? {...d,completed:d.completed} :{...d,completed:true}))
        })
    }
    editToDo(index, todo){
        this.setState({
            editIndex: index
        })
        this.setTodo(todo.todo);
    }
    setTodo(value){
        const {change} = this.props
        change("todo", value)
    }
    cancelEdit(){
        this.setState({
            editIndex: -1
        })
        this.setTodo('')
    }
    deleteAll(){
        this.setState({items: []});
    }
    completeAll(){
        this.setState({
            items: this.state.items.map(d=>({...d, completed: true}))
        })
    }
    setFilter(filter){
        this.setState({
            filter
        })
    }
  render() {
      const {handleSubmit}=this.props;
      const {items, editIndex, filter}=this.state;
    return (
      <div>
           <Form onSubmit={handleSubmit( this.submitfunction.bind(this))}>  
               {todoObject.map((field, key)=> (
              <Field component={Renderfield} key={key} {...{ ...field}}  />))}
                 <section>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                    {editIndex > -1 && <Button type="default"
                    onClick={this.cancelEdit.bind(this)}>
                        cancel
                    </Button>}
                    {editIndex == -1 && <Button type="default"
                    onClick={this.completeAll.bind(this)}>
                        complete all
                    </Button>}
                    {editIndex == -1 && <Button type="danger"
                    onClick={this.deleteAll.bind(this)}>
                        Delete All
                    </Button>}

                 </section>
                </Form>  
                {items.filter(d=>(filter==null || d.completed==filter)).map( (v, index)=>(<div>
                    <RenderItem data={v}
                     onRemove={this.removeToDo.bind(this, index)}
                     onEdit={this.editToDo.bind(this, index, v)}
                     oncomleted={this.oncomleted.bind(this,index)}
                     filter={filter}/>
                    />
                    </div>)
                )}
                <section>
                    <Button type="primary" onClick={this.setFilter.bind(this, null)}>
                        all
                    </Button>
                    <Button type="primary" onClick={this.setFilter.bind(this, true)}>
                        completed
                    </Button>
                    <Button type="primary" onClick={this.setFilter.bind(this, false)}>
                        active
                    </Button>

                </section>
      </div>
    )
  }
}
export default reduxForm({
    form: 'ToDO'
  })(TodoContiant)
