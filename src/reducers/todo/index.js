import * as handlers from './handlers'

export const todo = (state = {}, action) => {
    return handlers[action.type]
        ? handlers[action.type](state, action)
        : state;
};